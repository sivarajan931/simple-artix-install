c_disk() {
	while :; do
		lsblk
		echo "Enter the disk from the list to install the OS: "
		read df
		ls $df >>/dev/null
		if [ $? -eq 0 ]; then
			echo "Are you sure about it  this will format your entire disk based on your decision  [y/N] ? "
			read reply
			if [[ $reply =~ ^[Yy]$ ]]; then
				echo "choice_disk_file='${df}'" >>profile.sh
				break
			fi
		else
			echo "Invalid Disk Try again or spam ctrl+c to exit"
		fi
	done

	echo "Disk Formatting choice: "
	echo "
	1) use FDSIK
	2) use CGDISK
	3) I have formatted the disk already. But it is yet to be mounted
	4) I have formatted and mounted boot partition in /mnt/efi and root partition in /mnt
	5) Auto Formatting (CLEARS THE ENTIRE DISK!!!!) 0) exit"
	echo "Your Choice: "
	read n
	echo "choice_format=${n}" >>profile.sh

}
c_keymap() {
	echo -ne "
Please select key board layout from this list
    -by
    -ca
    -cf
    -cz
    -de
    -dk
    -es
    -et
    -fa
    -fi
    -fr
    -gr
    -hu
    -il
    -it
    -lt
    -lv
    -mk
    -nl
    -no
    -pl
    -ro
    -ru
    -sg
    -ua
    -uk
    -us
"
	echo "Your key boards layout:"
	read km
	kemaps=(by ca cf cz de dk es et fa fi fr gr hu il it lt lv mk nl no pl ro ru sg ua uk us)
	flag=0
	for str in ${kemaps[@]}; do
		if [[ $str == $km ]]; then
			echo "choice_keymap='$str'" >>profile.sh
			flag=1
		fi
	done
	if [[ $flag -eq 0 ]]; then
		echo "not found  try again"
		c_keymap
	fi

}
c_tz() {

	# Added this from arch wiki https://wiki.archlinux.org/title/System_time
	new_timezone="$(curl --fail https://ipapi.co/timezone)"
	echo -ne "System detected your timezone to be '$new_timezone' \n"
	echo -ne "Is this correct? yes/no:"
	read answer
	case $answer in
	y | Y | yes | Yes | YES)
		echo "choice_timezone='${new_timezone}'" >>profile.sh
		;;
	n | N | no | NO | No)
		echo "Please enter your timezone e.g. Europe/London :"
		read new_timezone
		var="/usr/share/zoneinfo/"
		var+="${new_timezone}"
		ls $var >>/dev/null
		status=$?
		if [[ $status -ne 0 ]]; then
			c_tz
		else
			echo "choice_timezone='${new_timezone}'" >>profile.sh
		fi
		;;
	*)
		echo "Wrong option. Try again"
		timezone
		;;
	esac

}

c_user() {
	# host name
	while :; do
		echo "Type the username (You will be prompted to type your password during the installation): "
		read myname

		if [[ -z $myname ]]; then
			echo "empty string please type again."
		else
			if [[ "$myname" =~ [^a-zA-Z0-9] ]]; then
				echo "Invalid user name. The user name can have alpha numeric characters only"
			else
				echo "choice_username='$myname'" >>profile.sh
				break
			fi
		fi
	done

}

c_host() {
	# host name
	while :; do
		echo "Type the host name: "
		read myhostname

		if [[ -z $myhostname ]]; then
			echo "empty string please type again."
		else
			if [[ "$myhostname" =~ [^a-zA-Z0-9_-] ]]; then
				echo "Invalid host name.A valid host name contain Alpha Numeric, Hyphen(-) and underscore (_) characters"
			else
				echo "choice_hostname='$myhostname'" >>profile.sh
				break
			fi
		fi
	done
}
choice_gpu() {

	echo "Choose your driver.
	0. Auto-detect.(Default)
	1. Nvidia driver.
	2. AMD.
	3. AMD ati(legacy Radeon GPU)
	4. Intel HD graphics
	5. vmware.
	6. dummy."
	read choice
	echo "choice_gpu_drivers=${choice}" >>profile.sh

}

create_profile() {

	echo "Do you like to use the default profile (Not recommended if you are going to use my default configuration set)?[y/N]"
	read n

	if [[ $n =~ ^[Yy]$ ]]; then
		cp default_profile.sh profile.sh
		return
	else
		touch profile.sh
		"Continuing with the profile creation.You can backup profile.sh file along with this version of script for your subsequent installation. "
	fi

	echo "Choose your Linux Kernel: "
	echo "
	1) linux-lts
	2) linux-zen
	3) linux-hardend
	4) linux
	(Default Choice 2)"
	echo "Note: Linux LTS is always installed by default in this script for troubleshooting purpose. Linux Kernel headers will also be installed by default for the particular kernel."
	echo "Your Choice: "
	read n
	echo "kernel_choice=${n}" >>profile.sh
	target+='linux-lts dkms linux-lts-headers git vim base base-devel glibc'
	if [[ $n -eq 3 ]]; then
		target+=' linux-hardend'
	elif [[ $n -eq 4 ]]; then
		target+=' linux'
	else
		target+=' linux-zen'
	fi
	c_disk

	echo "Choose your init System: "
	echo "
	1) OpenRC
	2) s6
	3) dinit
	(Default Choice 1)"
	echo "Your Choice: "
	read n

	if [[ $n -eq 1 ]]; then
		target+=' openrc elogind-openrc'

	elif [[ $n -eq 2 ]]; then
		target+=' s6-base s6 elogind-s6'
	elif [[ $n -eq 3 ]]; then
		target+=' dinit elogind-dinit'
	else
		target+=' openrc elogind-openrc'
		n=1
	fi
	echo "init_choice=${n}" >>profile.sh
	echo "choice_target='${target}'" >>profile.sh

	echo "Choose Desktop Choice"
	echo "Choose your Desktop environment:
		1) KDE Plasma
		2) Xfce Desktop
		3) Minimal
		(Default choice 1)
"
	read n
	echo "choice_de=${n}" >>profile.sh
	echo "Do you like to install all the firmware drivers for your system? (Recommended) [y/N]: "
	read reply
	echo "choice_firmware='${reply}'" >>profile.sh
	echo " Do you like to enable dual booting support? [y/N]"
	read n
	echo "choice_os_prober=${n}" >> profile.sh
	c_keymap
	c_tz
	c_user
	c_host
	choice_gpu

	echo "Do you need flatpak support? [y/N]: "
	read reply
	echo "choice_flatpak='${reply}'" >>profile.sh

}

launch_installer() {
	echo " Do you like to launch the install script (base-installer .sh) ? [y/N]"
	read n
	if [[ $n =~ ^[Yy]$ ]]; then
		bash base-installer.sh
	else
		echo "You can launch it manually by using 'bash base-installer.sh'"
	fi

}

create_profile
launch_installer
