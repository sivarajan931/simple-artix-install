# Artix Linux installer

A basic shell script to install Artix Linux with KDE plasma desktop/Xfce Desktop environment and other necessary applications for user on top of base installation. This is same as my simple-arch-installer but this version is modified for Artix Linux.

This is WIP Script please do not run it it is still not compatible with Artix.
# Key features
- User profiles. Now you can make your own profiles for installation. This will reduce the amount of manual intervention during runtime and for re-installs.
- Install Base Artix Linux quickly.
- Auto Format enables you to quickly format the entire partition and setup Artix Linux faster for you.(Allocates 512MB for Boot and rest for root)
- Installs Artix Linux minimal KDE plasma desktop/ Xfce Desktop Environment.
- Installs necessary Linux applications for the user (User's choice is asked to install applications).
- Sets up AUR helpers.
- Configures grub for dual boot.
- Configures bash and zsh shells.
- You can also install other packages that are needed.

# Contributions
All types of Contributions are welcome. Bugs can be reported by opening an issue for this project.

# How to run
This is just a simple guide for installation of base system for more information check out [Artix wiki](https://wiki.artixlinux.org/).
Be sure to have around 50 GB of hard-disk storage.
1. Check for internet connection.
```ping artixlinux.org```
Note:
- for Wi-Fi connections you may need to use iwctl for setting up your internet.
2. Install git by using the following command:
```pacman -Syy git```
3. Now clone this repository using git clone:
```git clone http://gitlab.com/sivarajan931/simple-artix-install```
4. Execution
- First execute ```sh make-profile.sh```
- Follow the steps to create your profile.
- Then run this command ```sh base-installer.sh```

5. Just follow the instructions given by the install scripts.
## Profile creation
First step will be to create your own profile for your installation. This can be backed up so that when you install Arch Linux next time, you can use your profile to make the install faster for you. Profiles are found at `/scripts/profile.sh`. Be sure to back that profile. So that you do not have to go through the entire installation process.

