#!/bin/bash

source '/scripts/profile.sh'
initial() {
	sed -i 's/^#ParallelDownloads = 5/ParallelDownloads = 5/' /etc/pacman.conf #Enable Parallel downloads.

	pacman -Syy
	echo "Installing network drivers..."
	n=$init_choice
	if [[ $n -eq 1 ]]; then
		pacman -S --noconfirm --needed networkmanager networkmanager-openrc
		rc-update add NetworkManager default

	elif [[ $n -eq 2 ]]; then
		pacman -S --noconfirm --needed networkmanager networkmanager-s6
		#s6-rc-bundle add default NetworkManager
		s6-service add default NetworkManager
		s6-db-reload
	elif [[ $n -eq 2 ]]; then
		pacman -S --noconfirm --needed networkmanager networkmanager-dinit
		dinitctl enable NetworkManager
	fi

}
# set time and timezone

timezone() {
	# Added this from arch wiki https://wiki.archlinux.org/title/System_time
	var="/usr/share/zoneinfo/"
	var+="${choice_timezone}"
	ln -sf $var "/etc/localtime"
	hwclock --systohc
	# set keymap
	echo "KEYMAP=$choice_keymap" >/etc/vconsole.conf
	# Locale-gen
	echo LANG=en_US.UTF-8 >/etc/locale.conf
	echo "en_US.UTF-8 UTF-8" >/etc/locale.gen
	locale-gen
}

pacman -Syy

#setting up host name

add_hostname() {
	myhostname=$choice_hostname
	echo "$myhostname" >/etc/hostname
	echo "127.0.0.1    localhost" >>/etc/hosts
	echo "::1          localhost" >>/etc/hosts
	echo "127.0.1.1    $myhostname.localdomain $myhostname" >>/etc/hosts
}

install_grub() {
	pacman -S --noconfirm --needed efibootmgr grub mtools dosfstools os-prober

	echo "Installing GRUB..."
	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
	status=$?
	if [[ $status -ne 0 ]]; then
		echo "Error occurred. Quitting the installation."
	fi
	reply=$choice_os_prober
	if [[ $reply =~ ^[Yy]$ ]]; then
		echo "GRUB_DISABLE_OS_PROBER=false" >>/etc/default/grub
	fi
	grub-mkconfig -o /boot/grub/grub.cfg
}

gpu_drivers() {
	# GPU drivers.

	choice=$choice_gpu_drivers

	if [[ $choice -eq 1 ]]; then
		pacman -S nvida-dkms nvidia nvidia-settings cuda --noconfirm
		nvidia-xconfig
	elif [[ $choice -eq 2 ]]; then
		pacman -S --noconfirm xf86-video-amdgpu
	elif [[ $choice -eq 3 ]]; then
		pacman -S --noconfirm xf86-video-ati
	elif [[ $choice -eq 4 ]]; then
		pacman -S --noconfirm xf86-video-intel libva-intel-driver libvdpau-va-gl libva-intel-driver libva-util vulkan-intel
		# lib32-vulkan-intel
	elif [[ $choice -eq 5 ]]; then
		pacman -S --noconfirm xf86-video-vmware
	elif [ $choice -eq 6 ]; then
		pacman -S --noconfirm xf86-video-dummy
	else
		if lspci | grep -E "NVIDIA|GeForce"; then
			pacman -S nvida-dkms nvidia nvidia-settings cuda --noconfirm --needed
		elif lspci | grep -E "Radeon"; then
			pacman -S xf86-video-amdgpu --noconfirm --needed
		elif lspci | grep -E "Integrated Graphics Controller"; then
			pacman -S libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils --needed --noconfirm
		elif grep -E "Intel Corporation UHD" <<<${gpu_type}; then
			pacman -S libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils lib32-mesa --needed --noconfirm
		fi
	fi
}

install_pulse_audio() {
	pacman -S --noconfirm --needed pulseaudio-alsa alsa-plugins pavucontrol pulseaudio alsa alsa-utils
}

install_pipewire() {
	pacman -S --noconfirm --needed pipewire pipewire-alsa pipewire-pulse
}
install_kde_plasma() {
	echo "Installing KDE plasma desktop environment (Minimal)..."
	pacman -S --noconfirm --needed git xorg sddm plasma konsole kcalc krunner kate firefox vlc ntfs-3g zsh dolphin ark kcalc spectacle packagekit-qt5 partitionmanager wget openssh htop neofetch mtools dosfstools sshfs unrar bash-completion python-pip gwenview okular elogind
	n=$init_choice
	if [[ $n -eq 1 ]]; then
		pacman -S --noconfirm --needed sddm-openrc
		rc-update add sddm default

	elif [[ $n -eq 2 ]]; then
		pacman -S --noconfirm --needed networkmanager sddm-s6
		# ln -s /etc/runit/sv/ssdm /run/runit/sddm
		#s6-rc-bundle add default sddm
		s6-service add default sddm
		s6-db-reload
	elif [[ $n -eq 3 ]]; then
		pacman -S --noconfirm --needed networkmanager sddm-dinit
		dinitctl enable sddm
	fi

}

install_minimal() {

	echo "Installing just xorg and other file system tools ..."
	sudo pacman -S --noconfirm --needed xorg openssh neofetch unrar bash-completion zsh iwd mtools dosfstools ntfs-3g wget python-pip

}

install_xfce() {
	sudo pacman -S --noconfirm --needed xorg lightdm openssh neofetch lightdm-gtk-greeter lightdm-gtk-greeter-settings xfce4 xfce4-goodies unrar bash-completion zsh firefox arc-gtk-theme arc-icon-theme vlc gparted mtools dosfstools htop packagekit-qt5 ntfs-3g wget qt lightdm python-pip network-manager-applet elogind
	if [[ $n -eq 1 ]]; then
		pacman -S --noconfirm --needed lightdm-openrc
		rc-update add lightdm default

	elif [[ $n -eq 2 ]]; then
		pacman -S --noconfirm --needed lightdm-s6
		s6-rc-bundle add default lightdm
	elif [[ $n -eq 3 ]]; then
		pacman -S --noconfirm --needed lightdm-dinit
		dinitctl enable lightdm
	fi
}

add_user() {
	echo "Changing root password"
	passwd

	myusername=$choice_username
	useradd -mG wheel $myusername
	echo "type password for $myusername"
	passwd $myusername
	echo "${myusername} ALL=(ALL) ALL" >>/etc/sudoers.d/${myusername}

}

#Executing function one by one
initial
timezone
add_hostname
add_user
install_grub
install_pulse_audio

if [[ $choice_de -eq 2 ]]; then
	install_xfce
	echo "Base System with Xfce has been installed."

elif [[ $choice_de -eq 3 ]]; then
	install_minimal
	echo "Base System has been installed"
else
	install_kde_plasma
	echo "Base System with KDE plasma has been installed."
fi

gpu_drivers

if [[ $choice_flatpak =~ ^[Yy]$ ]]; then
	pacman -S flatpak --noconfirm --needed

fi

read -p "Do you like to execute the  post-install.sh script (You can also run the script after reboot)? [y/N]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
	su $username -s /bin/sh post-install.sh
else
	echo "Thank you for using this script. If you get any errors in the operating system,pls troubleshoot by yourself and or ask your friends or artix linux subreddit/forums and notify the fix to me. If there is any problem in this scripts, you can open issue."

fi
